import babel from 'rollup-plugin-babel';

export default {
  entry: 'node_modules/simple-xdm/plugin.js',
  dest: 'atlassian-connect/all.js',
  moduleName: 'AP',
  format: 'iife',
  plugins: [
    babel()
  ]
};
