var AP = (function () {
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};











var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();





var defineProperty = function (obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var get$1 = function get$1(object, property, receiver) {
  if (object === null) object = Function.prototype;
  var desc = Object.getOwnPropertyDescriptor(object, property);

  if (desc === undefined) {
    var parent = Object.getPrototypeOf(object);

    if (parent === null) {
      return undefined;
    } else {
      return get$1(parent, property, receiver);
    }
  } else if ("value" in desc) {
    return desc.value;
  } else {
    var getter = desc.get;

    if (getter === undefined) {
      return undefined;
    }

    return getter.call(receiver);
  }
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};



var set$1 = function set$1(object, property, value, receiver) {
  var desc = Object.getOwnPropertyDescriptor(object, property);

  if (desc === undefined) {
    var parent = Object.getPrototypeOf(object);

    if (parent !== null) {
      set$1(parent, property, value, receiver);
    }
  } else if ("value" in desc && desc.writable) {
    desc.value = value;
  } else {
    var setter = desc.set;

    if (setter !== undefined) {
      setter.call(receiver, value);
    }
  }

  return value;
};

var LOG_PREFIX = "[Simple-XDM] ";
var nativeBind = Function.prototype.bind;
var util = {
  locationOrigin: function locationOrigin() {
    if (!window.location.origin) {
      return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    } else {
      return window.location.origin;
    }
  },
  randomString: function randomString() {
    return Math.floor(Math.random() * 1000000000).toString(16);
  },
  isString: function isString(str) {
    return typeof str === "string" || str instanceof String;
  },
  argumentsToArray: function argumentsToArray(arrayLike) {
    return Array.prototype.slice.call(arrayLike);
  },
  argumentNames: function argumentNames(fn) {
    return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
    .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
    .match(/([^\s,]+)/g) || [];
  },
  hasCallback: function hasCallback(args) {
    var length = args.length;
    return length > 0 && typeof args[length - 1] === 'function';
  },
  error: function error(msg) {
    if (window.console && window.console.error) {
      var outputError = [];

      if (typeof msg === "string") {
        outputError.push(LOG_PREFIX + msg);
        outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
      } else {
        outputError.push(LOG_PREFIX);
        outputError = outputError.concat(Array.prototype.slice.call(arguments));
      }
      window.console.error.apply(null, outputError);
    }
  },
  warn: function warn(msg) {
    if (window.console) {
      console.warn(LOG_PREFIX + msg);
    }
  },
  _bind: function _bind(thisp, fn) {
    if (nativeBind && fn.bind === nativeBind) {
      return fn.bind(thisp);
    }
    return function () {
      return fn.apply(thisp, arguments);
    };
  },
  each: function each(list, iteratee) {
    var length;
    var key;
    if (list) {
      length = list.length;
      if (length != null && typeof list !== 'function') {
        key = 0;
        while (key < length) {
          if (iteratee.call(list[key], key, list[key]) === false) {
            break;
          }
          key += 1;
        }
      } else {
        for (key in list) {
          if (list.hasOwnProperty(key)) {
            if (iteratee.call(list[key], key, list[key]) === false) {
              break;
            }
          }
        }
      }
    }
  },
  extend: function extend(dest) {
    var args = arguments;
    var srcs = [].slice.call(args, 1, args.length);
    srcs.forEach(function (source) {
      if ((typeof source === "undefined" ? "undefined" : _typeof(source)) === "object") {
        Object.getOwnPropertyNames(source).forEach(function (name) {
          dest[name] = source[name];
        });
      }
    });
    return dest;
  },
  sanitizeStructuredClone: function sanitizeStructuredClone(object) {
    var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
    var blackList = [Error, Node];
    var warn = util.warn;
    var visitedObjects = [];

    function _clone(value) {
      if (typeof value === 'function') {
        warn("A function was detected and removed from the message.");
        return null;
      }

      if (blackList.some(function (t) {
        if (value instanceof t) {
          warn(t.name + " object was detected and removed from the message.");
          return true;
        }
        return false;
      })) {
        return {};
      }

      if (value && (typeof value === "undefined" ? "undefined" : _typeof(value)) === 'object' && whiteList.every(function (t) {
        return !(value instanceof t);
      })) {
        if (visitedObjects.indexOf(value) > -1) {
          warn("A circular reference was detected and removed from the message.");
          return null;
        }

        visitedObjects.push(value);

        var newValue = void 0;

        if (Array.isArray(value)) {
          newValue = value.map(function (element) {
            return _clone(element);
          });
        } else {
          newValue = {};
          for (var name in value) {
            if (value.hasOwnProperty(name)) {
              var clonedValue = _clone(value[name]);
              if (clonedValue !== null) {
                newValue[name] = clonedValue;
              }
            }
          }
        }
        return newValue;
      }
      return value;
    }

    return _clone(object);
  }
};

var PostMessage = function () {
  function PostMessage(data) {
    classCallCheck(this, PostMessage);

    var d = data || {};
    this._registerListener(d.listenOn);
  }

  createClass(PostMessage, [{
    key: "_registerListener",
    value: function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }
      listenOn.addEventListener("message", util._bind(this, this._receiveMessage), false);
    }
  }, {
    key: "_receiveMessage",
    value: function _receiveMessage(event) {

      var handler = this._messageHandlers[event.data.type],
          extensionId = event.data.eid,
          reg = void 0;

      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }

      if (!handler || !this._checkOrigin(event, reg)) {
        return false;
      }

      handler.call(this, event, reg);
    }
  }]);
  return PostMessage;
}();

var _each = util.each;
var document$1 = window.document;

function $(sel, context) {

  context = context || document$1;

  var els = [];
  if (sel) {
    if (typeof sel === 'string') {
      var results = context.querySelectorAll(sel),
          arr_results = Array.prototype.slice.call(results);
      Array.prototype.push.apply(els, arr_results);
    } else if (sel.nodeType === 1) {
      els.push(sel);
    } else if (sel === window) {
      els.push(sel);
    } else if (typeof sel === 'function') {
      $.onDomLoad(sel);
    }
  }

  util.extend(els, {
    each: function each(it) {
      _each(this, it);
      return this;
    },
    bind: function bind(name, callback) {
      this.each(function (i, el) {
        this.bind(el, name, callback);
      });
    },
    attr: function attr(k) {
      var v;
      this.each(function (i, el) {
        v = el[k] || el.getAttribute && el.getAttribute(k);
        return !v;
      });
      return v;
    },
    removeClass: function removeClass(className) {
      return this.each(function (i, el) {
        if (el.className) {
          el.className = el.className.replace(new RegExp('(^|\\s)' + className + '(\\s|$)'), ' ');
        }
      });
    },
    html: function html(_html) {
      return this.each(function (i, el) {
        el.innerHTML = _html;
      });
    },
    append: function append(spec) {
      return this.each(function (i, to) {
        var el = context.createElement(spec.tag);
        _each(spec, function (k, v) {
          if (k === '$text') {
            if (el.styleSheet) {
              // style tags in ie
              el.styleSheet.cssText = v;
            } else {
              el.appendChild(context.createTextNode(v));
            }
          } else if (k !== 'tag') {
            el[k] = v;
          }
        });
        to.appendChild(el);
      });
    }
  });

  return els;
}

function binder(std, odd) {
  std += 'EventListener';
  odd += 'Event';
  return function (el, e, fn) {
    if (el[std]) {
      el[std](e, fn, false);
    } else if (el[odd]) {
      el[odd]('on' + e, fn);
    }
  };
}

$.bind = binder('add', 'attach');
$.unbind = binder('remove', 'detach');

$.onDomLoad = function (func) {
  var w = window,
      readyState = w.document.readyState;

  if (readyState === "complete") {
    func.call(w);
  } else {
    $.bind(w, "load", function () {
      func.call(w);
    });
  }
};

function getContainer() {
  // Look for these two selectors first... you need these to allow for the auto-shrink to work
  // Otherwise, it'll default to document.body which can't auto-grow or auto-shrink
  var container = $('.ac-content, #content');
  return container.length > 0 ? container[0] : document.body;
}

/**
* Extension wide configuration values
*/
var ConfigurationOptions = function () {
  function ConfigurationOptions() {
    classCallCheck(this, ConfigurationOptions);

    this.options = {};
  }

  createClass(ConfigurationOptions, [{
    key: "_flush",
    value: function _flush() {
      this.options = {};
    }
  }, {
    key: "get",
    value: function get(item) {
      return item ? this.options[item] : this.options;
    }
  }, {
    key: "set",
    value: function set(data, value) {
      var _this = this;

      if (!data) {
        return;
      }

      if (value) {
        data = defineProperty({}, data, value);
      }
      var keys = Object.getOwnPropertyNames(data);
      keys.forEach(function (key) {
        _this.options[key] = data[key];
      }, this);
    }
  }]);
  return ConfigurationOptions;
}();

var ConfigurationOptions$1 = new ConfigurationOptions();

var size = function size(width, height, container) {
  var w = width == null ? '100%' : width,
      h,
      docHeight;
  var widthInPx = Boolean(ConfigurationOptions$1.get('widthinpx'));
  container = container || getContainer();
  if (!container) {
    util.warn('size called before container or body appeared, ignoring');
  }

  if (widthInPx && typeof w === "string" && w.search('%') !== -1) {
    w = Math.max(container.scrollWidth, container.offsetWidth, container.clientWidth);
  }
  if (height) {
    h = height;
  } else {
    // Determine height of document element
    docHeight = Math.max(container.scrollHeight, document.documentElement.scrollHeight, container.offsetHeight, document.documentElement.offsetHeight, container.clientHeight, document.documentElement.clientHeight);

    if (container === document.body) {
      h = docHeight;
    } else {
      var computed = window.getComputedStyle(container);
      h = container.getBoundingClientRect().height;
      if (h === 0) {
        h = docHeight;
      } else {
        var additionalProperties = ['margin-top', 'margin-bottom'];
        additionalProperties.forEach(function (property) {
          var floated = parseFloat(computed[property]);
          h += floated;
        });
      }
    }
  }
  return { w: w, h: h };
};

function EventQueue() {
  this.q = [];
  this.add = function (ev) {
    this.q.push(ev);
  };

  var i, j;
  this.call = function () {
    for (i = 0, j = this.q.length; i < j; i++) {
      this.q[i].call();
    }
  };
}

function attachResizeEvent(element, resized) {
  if (!element.resizedAttached) {
    element.resizedAttached = new EventQueue();
    element.resizedAttached.add(resized);
  } else if (element.resizedAttached) {
    element.resizedAttached.add(resized);
    return;
  }

  element.resizeSensor = document.createElement('div');
  element.resizeSensor.className = 'ac-resize-sensor';
  var style = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
  var styleChild = 'position: absolute; left: 0; top: 0;';

  element.resizeSensor.style.cssText = style;
  element.resizeSensor.innerHTML = '<div class="ac-resize-sensor-expand" style="' + style + '">' + '<div style="' + styleChild + '"></div>' + '</div>' + '<div class="ac-resize-sensor-shrink" style="' + style + '">' + '<div style="' + styleChild + ' width: 200%; height: 200%"></div>' + '</div>';
  element.appendChild(element.resizeSensor);

  // https://bugzilla.mozilla.org/show_bug.cgi?id=548397
  // do not set body to relative
  if (element.nodeName !== 'BODY' && window.getComputedStyle && window.getComputedStyle(element).position === 'static') {
    element.style.position = 'relative';
  }

  var expand = element.resizeSensor.childNodes[0];
  var expandChild = expand.childNodes[0];
  var shrink = element.resizeSensor.childNodes[1];

  var lastWidth, lastHeight;

  var reset = function reset() {
    expandChild.style.width = expand.offsetWidth + 10 + 'px';
    expandChild.style.height = expand.offsetHeight + 10 + 'px';
    expand.scrollLeft = expand.scrollWidth;
    expand.scrollTop = expand.scrollHeight;
    shrink.scrollLeft = shrink.scrollWidth;
    shrink.scrollTop = shrink.scrollHeight;
    lastWidth = element.offsetWidth;
    lastHeight = element.offsetHeight;
  };

  reset();

  var changed = function changed() {
    if (element.resizedAttached) {
      element.resizedAttached.call();
    }
  };

  var onScroll = function onScroll() {
    if (element.offsetWidth !== lastWidth || element.offsetHeight !== lastHeight) {
      changed();
    }
    reset();
  };

  expand.addEventListener('scroll', onScroll);
  shrink.addEventListener('scroll', onScroll);

  var observerConfig = {
    attributes: true,
    attributeFilter: ['style']
  };

  var observer = new MutationObserver(onScroll);
  observer.observe(element, observerConfig);
}

var resizeListener = {
  add: function add(fn) {
    var container = getContainer();
    attachResizeEvent(container, fn);
  },
  remove: function remove() {
    var container = getContainer();
    if (container.resizeSensor) {
      container.removeChild(container.resizeSensor);
      delete container.resizeSensor;
      delete container.resizedAttached;
    }
  }
};

var AutoResizeAction = function () {
  function AutoResizeAction(callback) {
    classCallCheck(this, AutoResizeAction);

    this.dimensionStores = {
      width: [],
      height: []
    };
    this.callback = callback;
  }

  createClass(AutoResizeAction, [{
    key: '_setVal',
    value: function _setVal(val, type, time) {
      this.dimensionStores[type] = this.dimensionStores[type].filter(function (entry) {
        return time - entry.setAt < 400;
      });
      this.dimensionStores[type].push({
        val: parseInt(val, 10),
        setAt: time
      });
    }
  }, {
    key: '_isFlicker',
    value: function _isFlicker(val, type) {
      return this.dimensionStores[type].length >= 5;
    }
  }, {
    key: 'triggered',
    value: function triggered(dimensions) {
      dimensions = dimensions || size();
      var now = Date.now();
      this._setVal(dimensions.w, 'width', now);
      this._setVal(dimensions.h, 'height', now);
      var isFlickerWidth = this._isFlicker(dimensions.w, 'width', now);
      var isFlickerHeight = this._isFlicker(dimensions.h, 'height', now);
      if (isFlickerWidth) {
        dimensions.w = "100%";
        console.error("SIMPLE XDM: auto resize flickering width detected, setting to 100%");
      }
      if (isFlickerHeight) {
        var vals = this.dimensionStores['height'].map(function (x) {
          return x.val;
        });
        dimensions.h = Math.max.apply(null, vals) + 'px';
        console.error("SIMPLE XDM: auto resize flickering height detected, setting to: " + dimensions.h);
      }
      this.callback(dimensions.w, dimensions.h);
    }
  }]);
  return AutoResizeAction;
}();

var ConsumerOptions = function () {
  function ConsumerOptions() {
    classCallCheck(this, ConsumerOptions);
  }

  createClass(ConsumerOptions, [{
    key: "_getConsumerOptions",
    value: function _getConsumerOptions() {
      var options = {},
          $script = $("script[src*='/atlassian-connect/all']");

      if (!($script && /\/atlassian-connect\/all(-debug)?\.js($|\?)/.test($script.attr("src")))) {
        $script = $("#ac-iframe-options");
      }

      if ($script && $script.length > 0) {
        // get its data-options attribute, if any
        var optStr = $script.attr("data-options");
        if (optStr) {
          // if found, parse the value into kv pairs following the format of a style element
          optStr.split(";").forEach(function (nvpair) {
            nvpair = nvpair.trim();
            if (nvpair) {
              var nv = nvpair.split(":"),
                  k = nv[0].trim(),
                  v = nv[1].trim();
              if (k && v != null) {
                options[k] = v === "true" || v === "false" ? v === "true" : v;
              }
            }
          });
        }
      }

      return options;
    }
  }, {
    key: "_flush",
    value: function _flush() {
      delete this._options;
    }
  }, {
    key: "get",
    value: function get(key) {
      if (!this._options) {
        this._options = this._getConsumerOptions();
      }
      if (key) {
        return this._options[key];
      }
      return this._options;
    }
  }]);
  return ConsumerOptions;
}();

var ConsumerOptions$1 = new ConsumerOptions();

var POSSIBLE_MODIFIER_KEYS = ['ctrl', 'shift', 'alt', 'meta'];

var AP = function (_PostMessage) {
  inherits(AP, _PostMessage);

  function AP(options) {
    classCallCheck(this, AP);

    var _this = possibleConstructorReturn(this, (AP.__proto__ || Object.getPrototypeOf(AP)).call(this));

    ConfigurationOptions$1.set(options);
    _this._data = _this._parseInitData();
    ConfigurationOptions$1.set(_this._data.options);
    _this._host = window.parent || window;
    _this._isKeyDownBound = false;
    _this._hostModules = {};
    _this._eventHandlers = {};
    _this._pendingCallbacks = {};
    _this._keyListeners = [];
    _this._version = "%%GULP_INJECT_VERSION%%";
    _this._apiTampered = undefined;
    _this._isSubIframe = window.top !== window.parent;
    _this._onConfirmedFns = [];
    if (_this._data.api) {
      _this._setupAPI(_this._data.api);
      _this._setupAPIWithoutRequire(_this._data.api);
    }

    _this._messageHandlers = {
      presp: _this._handleResponse,
      evt: _this._handleEvent,
      key_listen: _this._handleKeyListen,
      api_tamper: _this._handleApiTamper
    };

    if (_this._data.origin) {
      _this._sendInit(_this._host);
      if (_this._isSubIframe) {
        _this._sendInit(window.top);
      }
    }
    _this._registerOnUnload();
    _this.resize = util._bind(_this, function (width, height) {
      if (!getContainer()) {
        util.warn('resize called before container or body appeared, ignoring');
        return;
      }
      var dimensions = size();
      if (!width) {
        width = dimensions.w;
      }
      if (!height) {
        height = dimensions.h;
      }
      if (_this._hostModules.env && _this._hostModules.env.resize) {
        _this._hostModules.env.resize(width, height);
      }
    });
    $(util._bind(_this, _this._autoResizer));
    _this.container = getContainer;
    _this.size = size;
    return _this;
  }

  createClass(AP, [{
    key: '_handleApiTamper',
    value: function _handleApiTamper(event) {
      if (event.data.tampered !== false) {
        this._host = undefined;
        this._apiTampered = true;
        console.error('XDM API tampering detected, api disabled');
      } else {
        this._apiTampered = false;
        this._onConfirmedFns.forEach(function (cb) {
          cb.apply(null);
        });
      }
      this._onConfirmedFns = [];
    }
  }, {
    key: '_registerOnUnload',
    value: function _registerOnUnload() {
      $.bind(window, 'unload', util._bind(this, function () {
        this._host.postMessage({
          eid: this._data.extension_id,
          type: 'unload'
        }, this._data.origin || '*');
      }));
    }
  }, {
    key: '_bindKeyDown',
    value: function _bindKeyDown() {
      if (!this._isKeyDownBound) {
        $.bind(window, 'keydown', util._bind(this, this._handleKeyDownDomEvent));
        this._isKeyDownBound = true;
      }
    }
  }, {
    key: '_autoResizer',
    value: function _autoResizer() {
      this._enableAutoResize = Boolean(ConfigurationOptions$1.get('autoresize'));
      if (ConsumerOptions$1.get('resize') === false) {
        this._enableAutoResize = false;
      }
      if (this._enableAutoResize) {
        this._initResize();
      }
    }

    /**
    * The initialization data is passed in when the iframe is created as its 'name' attribute.
    * Example:
    * {
    *   extension_id: The ID of this iframe as defined by the host
    *   origin: 'https://example.org'  // The parent's window origin
    *   api: {
    *     _globals: { ... },
    *     messages = {
    *       clear: {},
    *       ...
    *     },
    *     ...
    *   }
    * }
    **/

  }, {
    key: '_parseInitData',
    value: function _parseInitData(data) {
      try {
        return JSON.parse(data || window.name);
      } catch (e) {
        return {};
      }
    }
  }, {
    key: '_findTarget',
    value: function _findTarget(moduleName, methodName) {
      return this._data.options && this._data.options.targets && this._data.options.targets[moduleName] && this._data.options.targets[moduleName][methodName] ? this._data.options.targets[moduleName][methodName] : 'top';
    }
  }, {
    key: '_createModule',
    value: function _createModule(moduleName, api) {
      var _this2 = this;

      return Object.getOwnPropertyNames(api).reduce(function (accumulator, memberName) {
        var member = api[memberName];
        if (member.hasOwnProperty('constructor')) {
          accumulator[memberName] = _this2._createProxy(moduleName, member, memberName);
        } else {
          accumulator[memberName] = _this2._createMethodHandler({
            mod: moduleName,
            fn: memberName,
            target: _this2._findTarget(moduleName, memberName)
          });
        }
        return accumulator;
      }, {});
    }
  }, {
    key: '_setupAPI',
    value: function _setupAPI(api) {
      var _this3 = this;

      this._hostModules = Object.getOwnPropertyNames(api).reduce(function (accumulator, moduleName) {
        accumulator[moduleName] = _this3._createModule(moduleName, api[moduleName], api[moduleName]._options);
        return accumulator;
      }, {});

      Object.getOwnPropertyNames(this._hostModules._globals || {}).forEach(function (global) {
        _this3[global] = _this3._hostModules._globals[global];
      });
    }
  }, {
    key: '_setupAPIWithoutRequire',
    value: function _setupAPIWithoutRequire(api) {
      var _this4 = this;

      Object.getOwnPropertyNames(api).forEach(function (moduleName) {
        if (typeof _this4[moduleName] !== "undefined") {
          throw new Error('XDM module: ' + moduleName + ' will collide with existing variable');
        }
        _this4[moduleName] = _this4._createModule(moduleName, api[moduleName]);
      }, this);
    }
  }, {
    key: '_pendingCallback',
    value: function _pendingCallback(mid, fn) {
      this._pendingCallbacks[mid] = fn;
    }
  }, {
    key: '_createProxy',
    value: function _createProxy(moduleName, api, className) {
      var module = this._createModule(moduleName, api);
      function Cls(args) {
        if (!(this instanceof Cls)) {
          return new Cls(arguments);
        }
        this._cls = className;
        this._id = util.randomString();
        module.constructor.apply(this, args);
        return this;
      }
      Object.getOwnPropertyNames(module).forEach(function (methodName) {
        if (methodName !== 'constructor') {
          Cls.prototype[methodName] = module[methodName];
        }
      });
      return Cls;
    }
  }, {
    key: '_createMethodHandler',
    value: function _createMethodHandler(methodData) {
      var that = this;
      return function () {
        var args = util.argumentsToArray(arguments);
        var data = {
          eid: that._data.extension_id,
          type: 'req',
          mod: methodData.mod,
          fn: methodData.fn
        };

        var targetOrigin = '*';
        var target;
        if (that._findTarget(methodData.mod, methodData.fn) === 'top') {
          target = window.top;
        } else {
          target = that._host;
          targetOrigin = that._data.origin;
        }
        if (util.hasCallback(args)) {
          data.mid = util.randomString();
          that._pendingCallback(data.mid, args.pop());
        }
        if (this && this._cls) {
          data._cls = this._cls;
          data._id = this._id;
        }
        data.args = util.sanitizeStructuredClone(args);

        if (that._isSubIframe && typeof that._apiTampered === 'undefined') {
          that._onConfirmedFns.push(function () {
            target.postMessage(data, targetOrigin);
          });
        } else {
          target.postMessage(data, targetOrigin);
        }
      };
    }
  }, {
    key: '_handleResponse',
    value: function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        try {
          pendingCallback.apply(window, data.args);
        } catch (e) {
          util.error('exception thrown in callback', e);
        }
      }
    }
  }, {
    key: '_handleEvent',
    value: function _handleEvent(event) {
      var sendResponse = function sendResponse() {
        var args = util.argumentsToArray(arguments);
        event.source.postMessage({
          eid: this._data.extension_id,
          mid: event.data.mid,
          type: 'resp',
          args: args
        }, this._data.origin);
      };
      var data = event.data;
      sendResponse = util._bind(this, sendResponse);
      sendResponse._context = {
        eventName: data.etyp
      };
      function toArray$$1(handlers) {
        if (handlers) {
          if (!Array.isArray(handlers)) {
            handlers = [handlers];
          }
          return handlers;
        }
        return [];
      }
      var handlers = toArray$$1(this._eventHandlers[data.etyp]);
      handlers = handlers.concat(toArray$$1(this._eventHandlers._any));
      handlers.forEach(function (handler) {
        try {
          handler(data.evnt, sendResponse);
        } catch (e) {
          util.error('exception thrown in event callback for:' + data.etyp);
        }
      }, this);
      if (data.mid) {
        sendResponse();
      }
    }
  }, {
    key: '_handleKeyDownDomEvent',
    value: function _handleKeyDownDomEvent(event) {
      var modifiers = [];
      POSSIBLE_MODIFIER_KEYS.forEach(function (modifierKey) {
        if (event[modifierKey + 'Key']) {
          modifiers.push(modifierKey);
        }
      }, this);
      var keyListenerId = this._keyListenerId(event.keyCode, modifiers);
      var requestedKey = this._keyListeners.indexOf(keyListenerId);
      if (requestedKey >= 0) {
        this._host.postMessage({
          eid: this._data.extension_id,
          keycode: event.keyCode,
          modifiers: modifiers,
          type: 'key_triggered'
        }, this._data.origin);
      }
    }
  }, {
    key: '_keyListenerId',
    value: function _keyListenerId(keycode, modifiers) {
      var keyListenerId = keycode;
      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }
        modifiers.sort();
        modifiers.forEach(function (modifier) {
          keyListenerId += '$$' + modifier;
        }, this);
      }
      return keyListenerId;
    }
  }, {
    key: '_handleKeyListen',
    value: function _handleKeyListen(postMessageEvent) {
      var keyListenerId = this._keyListenerId(postMessageEvent.data.keycode, postMessageEvent.data.modifiers);
      if (postMessageEvent.data.action === "remove") {
        var index = this._keyListeners.indexOf(keyListenerId);
        this._keyListeners.splice(index, 1);
      } else if (postMessageEvent.data.action === "add") {
        // only bind onKeyDown once a key is registered.
        this._bindKeyDown();
        this._keyListeners.push(keyListenerId);
      }
    }
  }, {
    key: '_checkOrigin',
    value: function _checkOrigin(event) {
      var no_source_types = ['api_tamper'];
      if (event.data && no_source_types.indexOf(event.data.type) > -1) {
        return true;
      }

      if (this._isSubIframe && event.source === window.top) {
        return true;
      }

      return event.origin === this._data.origin && event.source === this._host;
    }
  }, {
    key: '_sendInit',
    value: function _sendInit(frame) {
      var targets;
      if (frame === window.top && window.top !== window.parent) {
        targets = ConfigurationOptions$1.get('targets');
      }

      frame.postMessage({
        eid: this._data.extension_id,
        type: 'init',
        targets: targets
      }, '*');
    }
  }, {
    key: 'sendSubCreate',
    value: function sendSubCreate(extension_id, options) {
      options.id = extension_id;
      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'sub',
        ext: options
      }, this._data.origin);
    }
  }, {
    key: 'broadcast',
    value: function broadcast(event, evnt) {
      if (!util.isString(event)) {
        throw new Error("Event type must be string");
      }

      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'broadcast',
        etyp: event,
        evnt: evnt
      }, this._data.origin);
    }
  }, {
    key: 'require',
    value: function require(modules, callback) {
      var _this5 = this;

      var requiredModules = Array.isArray(modules) ? modules : [modules],
          args = requiredModules.map(function (module) {
        return _this5._hostModules[module] || _this5._hostModules._globals[module];
      });
      callback.apply(window, args);
    }
  }, {
    key: 'register',
    value: function register(handlers) {
      if ((typeof handlers === 'undefined' ? 'undefined' : _typeof(handlers)) === "object") {
        this._eventHandlers = _extends({}, this._eventHandlers, handlers) || {};
        this._host.postMessage({
          eid: this._data.extension_id,
          type: 'event_query',
          args: Object.getOwnPropertyNames(handlers)
        }, this._data.origin);
      }
    }
  }, {
    key: 'registerAny',
    value: function registerAny(handlers) {
      this.register({ '_any': handlers });
    }
  }, {
    key: '_initResize',
    value: function _initResize() {
      this.resize();
      var autoresize = new AutoResizeAction(this.resize);
      resizeListener.add(util._bind(autoresize, autoresize.triggered));
    }
  }]);
  return AP;
}(PostMessage);

var plugin$1 = new AP();

return plugin$1;

}());
