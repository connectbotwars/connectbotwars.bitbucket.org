import babel from 'rollup-plugin-babel';

export default {
  entry: 'node_modules/simple-xdm/host.js',
  dest: 'atlassian-connect/host.js',
  moduleName: 'connectHost',
  format: 'iife',
  plugins: [
    babel()
  ]
};
