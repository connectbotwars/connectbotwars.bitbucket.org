var dispatch = throttle(connectHost.dispatch, 1000)
  .bind(connectHost);
var botSize = getBotDimensions();
var playfield = SVG(document.body)
  .size(innerWidth, innerHeight)
  .stroke('#0f0');
var bots;
var last;

connectHost.defineGlobals({
  go: function() {
    var context = getCallback(arguments)._context;
    context.element.dataset.going = "";
  },
  stop: function() {
    var context = getCallback(arguments)._context;
    delete context.element.dataset.going;
  },
  turn: function(degrees) {
    var context = getCallback(arguments)._context;
    var direction = getDataFloat(context.element, 'direction');
    var radians = parseFloat(degrees) * Math.PI / 180;
    var increment = Math.PI / 30;
    var wise = 1;
    var step = 1;
    if (typeof(context.turns) !== 'undefined') {
      context.turns.forEach(function(timeout) {
        clearTimeout(timeout);
      });
    }
    context.turns = [];
    if (radians < 0) {
      wise = -1;
      radians = -radians;
    }
    while (step * increment < radians) {
      turn(step * increment, step);
      step++;
    }
    turn(radians, step);

    function turn(ammount, step) {
      var timeout = setTimeout(function() {
        position(context.element, {
          direction: mod(direction + ammount * wise, 2 * Math.PI)
        });
        context.turns = remove(context.turns, timeout);
      }, step * 30);
      context.turns.push(timeout);
    }
  },
  getPosition(callback) {
    callback = getCallback(arguments);
    var context = callback._context;
    var degrees = mod(getDataFloat(context.element, 'direction') * 180 / Math.PI, 360);
    callback({
      top: getDataFloat(context.element, 'top'),
      left: getDataFloat(context.element, 'left'),
      direction: Math.round(degrees * 100) / 100
    });
  }
});

addBot({
  key: 'demo-bot',
  url: location.origin + '/bot.html'
});

bots = [].slice.call(document.getElementsByClassName('bot'));
last = performance.now();
requestAnimationFrame(loop);

function getBotDimensions() {
  var bounds;
  var dimensions = {};
  var probe = document.createElement('div');
  probe.className = 'bot';
  document.body.appendChild(probe);
  bounds = probe.getBoundingClientRect();
  document.body.removeChild(probe);
  dimensions.width = bounds.width;
  dimensions.height = bounds.height;
  return dimensions;
}

function addBot(options) {
  var iframeParams = connectHost.create({
    addon_key: options.key,
    url: options.url,
    key: 'avatar'
  });
  var iframe = document.createElement('iframe');
  iframe.setAttribute('id', iframeParams.id);
  iframe.setAttribute('src', iframeParams.src);
  iframe.setAttribute('name', iframeParams.name);
  iframe.setAttribute('class', 'bot');
  iframe.dataset.addon_key = options.key;
  position(iframe, {
    left: Math.random() * innerWidth - botSize.width,
    top: Math.random() * innerHeight - botSize.height
  });
  document.body.appendChild(iframe);
}

function loop(timestamp) {
  var lapsed = timestamp - last;
  var speed = lapsed / 10;
  bots.forEach(function(bot) {
    if (typeof(bot.dataset.going) !== 'undefined') {
      var direction = getDataFloat(bot, 'direction');
      var top = getDataFloat(bot, 'top') + Math.sin(direction) * speed;
      var left = getDataFloat(bot, 'left') + Math.cos(direction) * speed;
      var collision = false;
      if (top < 0) {
        top = 0;
        collision = true;
      } else if (top + botSize.height > innerHeight) {
        top = innerHeight - botSize.height;
        collision = true;
      }
      if (left < 0) {
        left = 0;
        collision = true;
      } else if (left + botSize.width > innerWidth) {
        left = innerWidth - botSize.width;
        collision = true;
      }
      position(bot, {
        top: top,
        left: left
      });
      if (collision) {
        dispatch('collision', {
          addon_key: bot.dataset.addon_key
        });
      }
    }
  });
  last = timestamp;
  requestAnimationFrame(loop);
}

function position(element, options) {
  element.dataset.top = get('top');
  element.dataset.left = get('left');
  element.dataset.direction = get('direction');
  element.style.transform = 'translate3d(' +
    element.dataset.left + 'px, ' +
    element.dataset.top + 'px, 0px)' +
    ' rotate(' + element.dataset.direction + 'rad)';

  function get(value) {
    return typeof(options[value]) === 'number' ?
      options[value].toFixed(2) : element.dataset[value] || 0
  }
}

function getDataFloat(element, datum) {
  return parseFloat(element.dataset[datum] || 0);
}

function getCallback(args) {
  var callback = args[args.length - 1];
  callback._context.element = document.getElementById(callback._context.extension_id);
  return callback;
}

function mod(n, m) {
  return ((n % m) + m) % m;
}

function remove(array, value) {
  return array.filter(function(element) {
    return element !== value;
  });
}

function throttle(func, wait) {
  var previous = 0;

  return function() {
    var now = Date.now();
    if (now - previous > wait) {
      previous = now;
      func.apply(this, arguments);
    }
  };
}
